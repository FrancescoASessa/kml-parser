<?php
declare(strict_types = 1);

namespace StepanDalecky\KmlParser\Entities;
use StepanDalecky\XmlElement\Element;

class MultiGeometry extends Entity
{
    /**
	 * @return Polygon[]
	 */
	public function getPolygons(): array
	{
		return array_map(function (Element $element) {
			return new Polygon($element);
		}, $this->element->getChildren('Polygon'));
	}
}
