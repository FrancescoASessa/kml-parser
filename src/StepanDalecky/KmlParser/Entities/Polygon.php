<?php
declare(strict_types = 1);

namespace StepanDalecky\KmlParser\Entities;

class Polygon extends Entity
{
	public function hasOuterBoundary(): bool
	{
		return $this->element->hasChild('outerBoundaryIs');
	}

	public function getOuterBoundaryCoordinates(): string
	{
		$outerBoundaryIs = $this->element->getChild('outerBoundaryIs');
		return (new LinearRing($outerBoundaryIs->getChild('LinearRing')))->getCoordinates();
	}

	public function hasInnerBoundary(): bool
	{
		return $this->element->hasChild('innerBoundaryIs');
	}

	public function getInnerBoundaryCoordinates(): string
	{
		$innerBoundaryIs = $this->element->getChild('innerBoundaryIs');
		return (new LinearRing($innerBoundaryIs->getChild('LinearRing')))->getCoordinates();
	}
}
