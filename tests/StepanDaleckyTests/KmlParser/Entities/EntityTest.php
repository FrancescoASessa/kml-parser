<?php
declare(strict_types=1);

namespace StepanDaleckyTests\KmlParser\Entities;

use PHPUnit\Framework\TestCase;
use StepanDalecky\KmlParser\Parser;

class EntityTest extends TestCase
{

	public function testGetElement()
	{
		$parentEntity = Parser::fromString('<parent><child>value</child></parent>');

		self::assertSame('value', $parentEntity->getElement()->getChild('child')->getValue());
	}
}
